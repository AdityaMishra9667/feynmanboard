import React, { useState } from "react"
import "./App.css"
import LandingPage from './components/landingPage';
import { BrowserRouter as Router, Route,Switch } from 'react-router-dom';
import Dashboard from './components/Dashboard';

export const CredentialsContext = React.createContext()

function App() {
  const credentials = useState(null)
  return (
    <div className="App">
      <CredentialsContext.Provider value={credentials}>
        <Router>  
            <Route path="/" exact component={LandingPage} />
            <Route path="/dashboard" exact component={Dashboard} />
        </Router> 
      </CredentialsContext.Provider>
    </div> 
  );
}

export default App;
