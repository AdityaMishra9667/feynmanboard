import React, { useContext, useEffect } from 'react'
import { CredentialsContext } from '../App'
import {useDispatch} from 'react-redux'
import { createNote } from '../actions/Notes'

function Dashboard() {
    const [credentials] = useContext(CredentialsContext) //getting the username and password
    const [content, setContent] = useState({username:"",password:"",notes:[{user:"",content:"",value:0}]})
    const dispatch = useDispatch()

    //fill the dashboard with the content of all the userNotes
    useEffect(() => {
        dispatch(fetchNote())        
    }, [dispatch])

    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch(createNote(content))
    }

    return (
        <div>
            <h1>Welcome {credentials && credentials.username}</h1>
            <form onSubmit={handleSubmit}>
                <input type="text" onChange={(e) => setContent({username:credentials.username, password:credentials.password,notes:[...notes,{user:credentials.username,content:e.target.value,value:0}]})} />
                <button type="submit">Submit</button>
            </form>
        </div>
    )
}

export default Dashboard
