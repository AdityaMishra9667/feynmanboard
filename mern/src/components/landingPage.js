import React, {useContext, useState} from 'react'
import {useHistory } from 'react-router-dom'
import { CredentialsContext } from '../App'

function LandingPage() {
    const[username, setUsername] = useState("")
    const[password, setPassword] = useState("")
    const[error, setError] = useState("")
    const [,setCredentials] = useContext(CredentialsContext)

    const handleErrors = async (response) => {
        if(!response.ok){
            const {message} = await response.json()
            throw Error(message)
        }

        return response.json()
    }

    const Register = (e) => {
        e.preventDefault();       
        fetch('http://localhost:5000/register', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                username,
                password
            })
        })
        .then(handleErrors)
        .then(() => {
            setCredentials({
                username:username,
                password: password
            });
            history.push('/dashboard')
        })
        .catch((error) => {
            setError(error.message)
        })
    } 

    const Login = (e) => {
        e.preventDefault()         
        fetch('http://localhost:5000/login', {
            method: "POST",
            redirect: "follow",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                username,
                password
            })
        }) 
        .then(handleErrors)
        .then(() => {
            setCredentials({
                username:username,
                password:password
            });
            history.push('/dashboard')
        })         
        .catch((error) => {
            setError(error.message)
        })       
    }

    const history = useHistory()

    return (
        <div>
            {error && <span style={{color: "red"}}>{error}</span>}
            <h1>Register/Login</h1>
            <form onSubmit={Register}>
                <input onChange={(e) => setUsername(e.target.value)} placeholder="username" />
                <br />
                <input type="password" onChange={(e) => setPassword(e.target.value)} placeholder="password" />
                <br />
                <button type="submit">Register</button>
            </form>
            <form onSubmit={Login}>
                <button>Login</button>
            </form>
        </div>
    )
}

export default LandingPage
