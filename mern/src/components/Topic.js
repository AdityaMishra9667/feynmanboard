import React, { useState } from 'react'
import { CredentialsContext } from '../App'
import {useDispatch, useSelector} from 'react-redux'
import "./Topic.css"

function Topic(currentId) {
    //extracting all the user notes
    //the data is of the form:-
    //username: String,
    // password: String,
    // notes:[{
    //     user: String,
    //     content: String,
    //     value: Number
    // }]
    const userNote = useSelector((state) => currentId ? state.userNotes.find((p) => p._id === currentId): null)
    const posts = userNote.notes.content.split(/[/;:?.|-,()[]{}'"]+/) //categorizing the content

    const [credentials] = useContext(CredentialsContext)
    const [content, setContent] = useState({username:userNote.username,password:userNote.password,notes:[...userNote.notes]})
    const dispatch = useDispatch()

    const[border, setBorder] = useState(false)

    const handleClick= (e) => {
        e.preventDefault()
        setBorder(!border)
    }

    const handleEdit = (e) =>{
        e.preventDefault()
        dispatch(updateNote(content))
    }

    return (
        //here i am returning each line inside a div container so that
        //i can style each line in css to apply borders and change colors
        <div>
            {posts.map((post) => {return <div className= {border ? "content border":"content"} onClick={handleClick}>{post}</div>})}
            <form onSubmit={handleSubmit}>
                <input type="text" onChange={(e) => setContent({username:credentials.username, password:credentials.password,notes:[...notes,{user:credentials.username,content:e.target.value,value:0}]})} />
                <button onClick={handleEdit}>Edit</button> 
            </form>
        </div>
    )
}

export default Topic
