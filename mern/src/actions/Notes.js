import * as api from '../api'

export const FETCH_ALL = 'FETCH_ALL'
export const CREATE = 'CREATE'
export const UPDATE = 'UPDATE'

export const fetchNotes = () => async (dispatch) => {
    try {
        const { data } = await api.fetchNotes()

        dispatch({type: FETCH_ALL, payload: data})
    } catch (error) {
        console.log(error)
    } 
}

export const createNote = (note) => async (dispatch) => {  
    try {
        const { data } = await api.createNote(note)

        dispatch({type: CREATE, payload: data})
    } catch (error) {
        console.log(error)
    }
}

export const updateNote = (id, note) => async(dispatch) => {
    try {
        const {data} = await api.updateNote(id, note)

        dispatch({type:UPDATE, payload: data})
    } catch(error){
        console.log(error)
    }
}