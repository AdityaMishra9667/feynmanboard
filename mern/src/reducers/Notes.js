export default (userNotes = [], action) => {
    switch(action.type){
        case UPDATE:
            return userNotes.map((note) => note._id === action.payload._id ? action.payload : note)

        case CREATE:
            return [...userNotes, action.payload]

        case FETCH_ALL:
            return action.payload
    }
    
    
}