import express from 'express'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'
import cors from 'cors'


const app = express()



app.use(bodyParser.json({limit: "30mb", extended: true}))
app.use(bodyParser.urlencoded({limit: "30mb", extended: true}))
app.use(cors())

//Model(Table)
const userSchema = mongoose.Schema({
    username: String,
    password: String,
    notes:[{
        user: String,
        content: String,
        value: Number
    }]
})

const UserNotes = mongoose.model('UserNotes', userSchema)

// Handle Requests
app.post("/register", async (req, res) => {
    const { username, password } = req.body;
    const user = await UserNotes.findOne({username});
    if (user)
    {
        res.status(500)
        res.send({
            message:"user exists"
        })
    }else{
        await UserNotes.create({username, password})
        res.send({
            message: "Success"
        })
        // res.redirect("/dashboard")
    }

})

app.post("/login", async (req, res) => {
    const { username, password } = req.body;
    const user = await UserNotes.findOne({username});
    if (user && user.password === password)
    {
        res.send({
            message:"success"
        })
        // res.redirect("/dashboard")
    }else{
        res.status(401)
        res.send({
            message:"invalid username or password"
        })
    }

})

app.get("/dashboard", async(req, res) => {
    const {username} = req.body
    try{
        const notes = await UserNotes.find({"notes.user":`${username}`})
        res.status(200).json([notes])
    }
    catch (error) {
        res.status(409).json({message: error.message})
    }
})

app.post("/notes", async(req, res) => {
    const note = req.body
    const newUserNote = new UserNotes(note)
    try{
        await newUserNote.save()
        res.status(201).json(newUserNote)
    }
    catch (error) {
        res.status(409).json({message: error.message})
    }
})

// Database Connection 
const PORT = process.env.PORT || 5000

mongoose.connect(process.env.CONNECTION_URL, {useNewUrlParser: true, useUnifiedTopology:true})
    .then(() => app.listen(PORT, () => console.log(`Server running on port ${PORT}`)))
    .catch((error) => console.log(error.message))

mongoose.set('useFindAndModify', false)


